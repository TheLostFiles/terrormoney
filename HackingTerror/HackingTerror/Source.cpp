
#include <iostream>

using namespace std;

void badSwap(int x, int y);
void goodSwap(int& x, int& y);

void main()
{
	cout << "Welcome to the Hacking Magic\n We are going to steal all the Money from Isis.\n";
	int JoesMoney = 15;
	int terrorMoney = 10000000;
	cout << "Original values\n";
	cout << "My Bank Account: $" << JoesMoney << "\n";
	cout << "Isis Bank Account: $" << terrorMoney << "\n\n";

	badSwap(JoesMoney, terrorMoney);
	cout << "My Bank Account: $" << JoesMoney << "\n";
	cout << "Isis Bank Account: $" << terrorMoney << "\n\n";

	goodSwap(JoesMoney, terrorMoney);
	cout << "My Bank Account: $" << JoesMoney << "\n";
	cout << "Isis Bank Account: $" << terrorMoney << "\n";
	cout << "We did it we stole all of Isis's Money.\n";
}

void badSwap(int x, int y)
{
	int temp = x; //temporary int for holding the money
	x = y; // swapping the money
	y = temp; // making the Y the money
}

void goodSwap(int& x, int& y)
{
	int temp = x;//temporary int for holding the money
	x = y;// swapping the money
	y = temp;// making the Y the money
}
